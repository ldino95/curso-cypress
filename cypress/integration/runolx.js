// testes
describe("Olx fazendo buscas", () => {
    beforeEach(() => {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })//ignora erros da página
        cy.visit("https://sp.olx.com.br/imoveis")}); //comando para executar site (cy.visit)
    
    it("procurando imoveis na olx busca-direta", () => {
       
        cy.get('#search-by-word-container > div.undefined.false.sc-11ykbmp-0.ioAVKB > form > div:nth-child(1) > div > input')
        .type('apartamento{enter}'); //buscar imovel direto na pesquisa

    });

        
    it.only("procurando imoveis com detalhes ", () => {
       
        cy.get('#column-main-content > div:nth-child(2) > div > div.sc-hmzhuo.sc-1ncgzjx-0.bGtMjx.sc-jTzLTM.iwtnNi > div > div:nth-child(1) > div:nth-child(1) > a')
        .click();//selecionar a cidade
        cy.get('#column-main-content > div:nth-child(2) > div > div.sc-hmzhuo.sc-1ncgzjx-0.bGtMjx.sc-jTzLTM.iwtnNi > div > div:nth-child(2) > div > div > div:nth-child(3) > a')
        .click();//selecionar bairros
        cy.get('#column-main-content > div:nth-child(2) > div > div.sc-hmzhuo.sc-1ncgzjx-0.bGtMjx.sc-jTzLTM.iwtnNi > div > button')
        .click();//abrindo filtro
        cy.get("#location-2966").check();// check box para escolher cidade ou bairro
        cy.xpath("/html/body/div[3]/div/div/div/div[4]/button[1]").click(); // clicar no filtrar
        cy.xpath('//*[@id="left-side-main-content"]/div[2]/div/div/div[2]/div/div/div[1]/div/div[2]/a/div/span[1]').click();
        cy.xpath('//*[@id="ad-list"]/li[1]/a/div/div[2]/div[1]/div[1]/h2').click();

    });

});

