/// <reference types="Cypress"/> 
describe("Olx fazendo buscas", () => {
    beforeEach(() => {
        Cypress.on('uncaught:exception', (err, runnable) => {
            return false
        })//ignora erros da página
        cy.visit("https://sp.olx.com.br")
    }); //comando para executar site (cy.visit)



    it("acessar página de login e digitar senha incorreta ", () => {

        const email = "luuhsousa2012@gmail.com";
       
        
        cy.get('.sc-fYiAbW').click();
        //.type('apartamento{enter}'); //buscar imovel direto na pesquisa
        cy.xpath('//*[@id="__next"]/div/div[1]/div[1]/div[2]/form/div[1]/div[2]/input').type(email);
        cy.get('.sc-ebFjAB').type('12345122');
        cy.get('.sc-kGXeez').click();

        //validar texto
        cy.get('.sc-etwtAo').should('have.text', 'Sua senha está incorreta.');


    });

    it("acessar página de login e digitar login incorreto ", () => {

        cy.get('.sc-fYiAbW').click();
        cy.xpath('//*[@id="__next"]/div/div[1]/div[1]/div[2]/form/div[1]/div[2]/input').type('lucassssemail');
        cy.get('.sc-ebFjAB').type("senha123")
        cy.get('.sc-kGXeez').click();

        //validar texto com xpath
        cy.xpath('//*[@id="__next"]/div/div[1]/div[1]/div[2]/form/div[1]/div[3]/span').should('have.text', ' Por favor, insira um endereço de e-mail válido. ');
    });

    it("Realizar pesquisa com dado invalido", () => {
        const produto = "sor afgafa"

        cy.get('#search-by-word-container > div.undefined.false.sc-11ykbmp-0.ioAVKB > form > div:nth-child(1) > div > input').type(produto);
        cy.get('.submitBtn').click();
        cy.get('.sc-145t6x-1').should('have.text','Nenhum anúncio foi encontrado.')
    });


    it("Realizar pesquisa com dado valido", () => {
        const produto = "sofa vermelho"

        cy.get('#search-by-word-container > div.undefined.false.sc-11ykbmp-0.ioAVKB > form > div:nth-child(1) > div > input').type(produto);
        cy.get('.submitBtn').click();
        cy.get('#ad-list > li.sc-1fcmfeb-2.OgCqv > a > div > div.sc-1q8ortj-0.gIkEbD > div.sc-101cdir-2.kBCTPf > img').click();
    });



});

