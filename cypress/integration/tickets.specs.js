// testes
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")); //comando para executar site (cy.visit)

    it("fills all the text input fields", () => {
        //exmeplos de input get
        const nome = "Lucas";
        const sobrenome = "Sousa";


        cy.get("#first-name").type(nome);
        cy.get("#last-name").type(sobrenome);
        cy.get("#email").type("lucas.sousa@email.com");
        cy.get("#requests").type("irmao");
        cy.get("#signature").type(`${nome} ${sobrenome}`);
    });

    //usando campo select
    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    //usando campo radionbutton
    it("Selecionando campo vip", () => {
        cy.get("#vip").check();

    });

    //usando campo radionbutton
    it("Selecionando checkbocks", () => {
        cy.get("#social-media").check();

    });

    it("Selecionando checkbocks", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#publication").uncheck();

    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");

    });

    it("Email não existe", () => {
        cy.get("#email")
            .as("email")
            .type("lucas.sousa-email.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("lucas.sousa@email.com")

        cy.get("#email.invalid").should("not.exist")

    });

    it("preencher todos os campos, e resetar em seguida", () => {

        const nome = "Lucas";
        const sobrenome = "Sousa";
        const fullName = `${nome} ${sobrenome}`;

        cy.get("#first-name").type(nome);
        cy.get("#last-name").type(sobrenome);
        cy.get("#email").type("lucas.sousa@email.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#requests").type("irmao");
        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`
        );
        cy.get("#agree").click();
        cy.get("#signature").type(fullName); 

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();
        cy.get("@submitButton").should("be.disabled")

    });

    it("Usando suporte e preenchendo campos mandatorios", () => {
        const acao = {
            nome: "joao",
            sobrenome:"sousa",
            email:"joao.sousa@mail.com"
        };

    cy.camposMandatorios(acao);
    cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");

    cy.get("#agree").uncheck();
    cy.get("@submitButton").should("be.disabled")
    
    });

});

