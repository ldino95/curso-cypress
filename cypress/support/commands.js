/// <reference types="Cypress"/> 
Cypress.Commands.add("camposMandatorios", data => {
    cy.get("#first-name").type(data.nome);
    cy.get("#last-name").type(data.sobrenome);
    cy.get("#email").type(data.email);
    cy.get("#agree").click();
});

Cypress.Commands.add('login', () => {
    const login = "standard_user";
    const senhar = "secret_sauce";


    cy.get('#login_credentials').should('contain.text', 'Accepted usernames are:');
    cy.get('[data-test=username]').type(login);
    cy.get('[data-test=password]').type(senhar);
    cy.get('[data-test=login-button]').click();

})


Cypress.Commands.add('logout', () => {
    cy.get('#react-burger-menu-btn').click();
    cy.get('#logout_sidebar_link').click();
    cy.get('#login_credentials').should('contain.text', 'Accepted usernames are:');

})

Cypress.Commands.add('adcionar', () =>{
    cy.get('[data-test=add-to-cart-sauce-labs-backpack]').click();
    cy.get('[data-test=add-to-cart-sauce-labs-bike-light]').click();
    cy.get('[data-test=add-to-cart-sauce-labs-onesie]').click();
})

Cypress.Commands.add('carrinho', () =>{
   cy.get('.shopping_cart_link').click();
   cy.url().should('eq','https://www.saucedemo.com/cart.html');
})